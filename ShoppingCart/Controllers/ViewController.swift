//
//  ViewController.swift
//  ShoppingCart
//
//  Created by ios developer on 24/05/2018.
//  Copyright © 2018 ios developer. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var usernameField: UITextField!
    
    @IBOutlet weak var passwordField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onSignInPressed(_ sender: UIButton) {
        self.performSegue(withIdentifier: "main", sender: self)
    }
    
}

